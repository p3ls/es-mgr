
Beispiel

```yaml
---
journalbeat.inputs:
- paths: 
  - "/var/log/journal/c948ab86f93b43ab8d7b78983f2a2bbf/user-1201.journal"
  seek: cursor
output.elasticsearch:
  hosts: ["https://127.0.0.1:9200"]
  ssl.certificate_authorities: ["/etc/journalbeat/elasticsearch-ca.pem"]
  username: "generic_index_writer"
  password: "xxx"
  index: "%{[process.name]}-%{+yyyy.MM.dd}"

processors:
  - decode_json_fields:
      fields: ["message"]
      process_array: false
      max_depth: 1
      target: ""
      overwrite_keys: false
      add_error_key: true
```

