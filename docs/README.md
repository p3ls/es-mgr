
# Komponenten unseres "Elastic-Stack" (aka ELK) - Servers

    ┌───────────────────┐┌───────────────────┐┌───────────────────┐┌───────────────────┐
    │                   ││                   ││                   ││                   │
    │   Elasticsearch   ││     Logstash      ││      Kibana       ││      es-mgr       │
    │                   ││                   ││                   ││                   │
    │                   ││                   ││                   ││                   │
    └───────────────────┘└───────────────────┘└───────────────────┘└───────────────────┘
    ┌──────────────────────────────────────────────────────────────────────────────────┐
    │                                                                                  │
    │                                  192.168.253.56                                  │
    │                                                                                  │
    └──────────────────────────────────────────────────────────────────────────────────┘

## Wofür steht ELK?

* E - Elasticsearch
* L - Logstash
* K - Kibana

Später kamen weitere Komponenten hinzu (hauptsächlich die sogenannten "Beats" - Log Driver) und deshalb hat man die Bezeichnung offiziell zu "Elastic - Stack" geändert.

    ┌───────────────────┐         ┌───────────────────┐
    │                   │         │                   │
    │     Logstash      │         │                   │
    │                   │────────▶│                   │
    │                   │         │                   │
    └───────────────────┘         │                   │
    ┌───────────────────┐         │                   │
    │                   │         │                   │
    │      Kibana       │         │   Elasticsearch   │
    │                   │◀────────│                   │
    │                   │         │                   │
    └───────────────────┘         │                   │
    ┌───────────────────┐         │                   │
    │                   │         │                   │
    │      es-mgr       │         │                   │
    │                   │◀───────▶│                   │
    │                   │         │                   │
    └───────────────────┘         └───────────────────┘

## Elasticsearch

* Datenbank vom Typ: Document-Store 

* Query Language: [KQL](https://www.elastic.co/guide/en/kibana/master/kuery-query.html)

* Kommunikation läuft immer über eine REST-API (Default-Port: 9200)
 
* mehrere Instanzen (Nodes) können sich zu einem Cluster zusammenschliessen

[Hier](es-basics.md) gibts mehr technische Details, die ich während der Recherche notiert habe.

## Logstash

Nimmt ausschlisslich Log-Daten entgegen, puffert sie, verändert/filtert/parst/erweitert sie optional und schickt sie zur Datenbank

* öffnet für jeden einzelnen "Input" einen Port, der Daten in einem definierten Format erwartet.




## Kibana

* Dashboard zur Administration der Datenbank
* Logs durchsuchen
* Metriken visualisieren
* ...

Aktuelle Adresse: http://192.168.253.56:5601/

Read-Only-User zum Logs anschauen:

    logs_viewer
    xxx




## es-mgr: (Elasticsearch Manager)

Client zur REST-API der Datenbank, koordiniert das Lifecycle-Management.

Funktionen im Detail:

* _configs
* _restore
* _archive
* _stats


## Log Driver

Log Driver sind kleine Zusatzprogramme und helfen dabei, Logs zur Datenbank zu transportieren, damit die Apps dafür keinen oder weniger Extra-Code einbauen müssen.

In gewisser Weise ist Logstash ein grosser Log Driver, obwohl kleinere Log-Driver ihre Daten auch an Logstash schicken können. Es geht hier hauptsächlich um die Lastverteilung.


# Welche Möglichkeiten gibt es Logs in die Datenbank zu speichern?

Jeder Log Driver sendet letzlich an den [_bulk](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html) - Endpoint der REST-API von Elasticsearch. Das kann man auch selbst machen, wird aber nicht empfohlen. Hauptsächlich muss man darauf achten, dass nicht zuviele Requests direkt an die Datenbank gestellt werden. [Bibliotheken](https://github.com/interactive-solutions/go-logrus-elasticsearch), die man in eigenem Code verwenden kann, federn das ab indem sie erst eine bestimmte Menge an Events sammeln um sie dann in einem Request zuverschicken.

* App sendet selbst nach ES via REST-API
    * Nachteile:
        * App muss relativ grosse Abhängigkeiten reinziehen (Elasticsearch Bibliothek, SSL-Cert, API-Key, User Login)
        * App-Crashes werden nicht geloggt
    * Vorteile:
        * App ist am Ende portabel und überall lauffähig (in Go könnten alle genannten Abhängigkeiten mit in eine einzige Binary kompiliert werden)

* App sendet selbst nach Logstash via Logstash - Input. 
    * [Beispiel in PHP: Senden an einen UDP - Port von Logstash](logstash.md)
    * [Beispiel in Go: Senden im GELF - Format für Graylog - Hooks](logstash-go-gelf.md)


* App und Log Driver auf einem System integrieren, z.B. [Transport von bestehenden Log Files mit Filebeat](filebeat.md)

    * https://github.com/gravitational/udpbeat (deprecated)
    * https://goteleport.com/blog/golang-error-handling/
    * https://www.elastic.co/guide/en/beats/libbeat/master/community-beats.html

* übergeordneter Prozess ([init-System](https://de.wikipedia.org/wiki/Systemd), [Supervisor](http://supervisord.org/)) registriert Events der App via `stdout`/`stderr` und sendet sie an Log Driver, z.B.
    * [Journalbeat](https://www.elastic.co/guide/en/beats/journalbeat/current/index.html)
        * Vorteile:
            * loggt Crashes
            * absolut kein Setup und keine Abhängigkeiten in der App notwendig, kein Hook, keine ES Library, keine Connection Details
            * `JSON` in Message - String wird erkannt und indiziert wenn vorhanden
        * Nachteile:
            * App muss korrekt als `systemd` - Unit eingebunden werden
            * expliziter App-User notwendig, `journald` gruppiert Journals nach UID's
            
    * Docker




## 





