
# Features

* Elasticsearch is the distributed search and analytics engine

* Logstash and Beats facilitate collecting, aggregating, and enriching your data and storing it in Elasticsearch

* Kibana enables you to interactively explore, visualize, and share insights into your data and manage and monitor the stack

* Elasticsearch is where the indexing, search, and analysis magic happens

* Elasticsearch provides near real-time search and analytics for all types of data. Whether you have structured or unstructured text, numerical data, or geospatial data, Elasticsearch can efficiently store and index it in a way that supports fast searches

* You can go far beyond simple data retrieval and aggregate information to discover trends and patterns in your data. 

* And as your data and query volume grows, the distributed nature of Elasticsearch enables your deployment to grow seamlessly right along with it

* Elasticsearch is a distributed document store

* Elasticsearch uses a data structure called an inverted index that supports very fast full-text searches

* An inverted index lists every unique word that appears in any document and identifies all of the documents each word occurs in

* An index can be thought of as an optimized collection of documents and each document is a collection of fields, which are the key-value pairs that contain your data

* By default, Elasticsearch indexes all data in every field and each indexed field has a dedicated, optimized data structure

* Elasticsearch will detect and map booleans, floating point and integer values, dates, and strings to the appropriate Elasticsearch data types

* You can define rules to control dynamic mapping and explicitly define mappings to take full control of how fields are stored and indexed

* While you can use Elasticsearch as a document store and retrieve documents and their metadata, 

* the real power comes from being able to easily access the full suite of search capabilities built on the Apache Lucene search engine library

* Elasticsearch provides a simple, coherent REST API for managing your cluster and indexing and searching your data

* structured queries, full text queries, and complex queries that combine the two

* Structured queries are similar to the types of queries you can construct in SQL

* You can add servers (nodes) to a cluster

* Under the covers, an Elasticsearch index is really just a logical grouping of one or more physical shards, where each shard is actually a self-contained index

* By distributing the documents in an index across multiple shards, and distributing those shards across multiple nodes, Elasticsearch can ensure redundancy

* There are two types of shards: primaries and replicas. Each document in an index belongs to one primary shard. A replica shard is a copy of a primary shard

* Aim to keep the average shard size between a few GB and a few tens of GB. For use cases with time-based data, it is common to see shards in the 20GB to 40GB range.

* Avoid the gazillion shards problem. The number of shards a node can hold is proportional to the available heap space. As a general rule, the number of shards per GB of heap space should be less than 20

* For performance reasons, the nodes within a cluster need to be on the same network. Balancing shards in a cluster across nodes in different data centers simply takes too long

* When you monitor a cluster, you collect data from the Elasticsearch nodes, Logstash nodes, Kibana instances, and Beats in your cluster. You can also use Filebeat to collect Elasticsearch logs.

* In production, we strongly recommend using a separate monitoring cluster

* Using a separate monitoring cluster prevents production cluster outages from impacting your ability to access your monitoring data

* A snapshot is a backup taken from a running Elasticsearch cluster. You can take snapshots of an entire cluster, including all its data streams and indices. You can also take snapshots of only specific data streams or indices in the cluster.

* Snapshots can be stored in either local or remote repositories

* Snapshots are incremental

* You can use snapshot lifecycle management to automatically take and manage snapshots.

* You cannot back up an Elasticsearch cluster by simply copying the data directories of all of its nodes

* Index Modules are modules created per index and control all aspects related to an index.

* Index level settings can be set per-index. Settings may be: static, dynamic

* Each index in Elasticsearch is divided into shards and each shard can have multiple copies. 

These copies are known as a replication group and must be kept in sync when documents are added or removed. If we fail to do so, reading from one copy will result in very different results than reading from another. The process of keeping the shard copies in sync and serving reads from them is what we call the data replication model.

* A data stream lets you store append-only time series data across multiple indices while giving you a single named resource for requests. Data streams are well-suited for logs, events, metrics, and other continuously generated data.

https://www.elastic.co/guide/en/elasticsearch/reference/7.9/data-streams.html

* Mapping is the process of defining how a document, and the fields it contains, are stored and indexed. For instance, use mappings to define:

  * which string fields should be treated as full text fields.

  * which fields contain numbers, dates, or geolocations.

  * the format of date values.

  * custom rules to control the mapping for dynamically added fields.

https://www.elastic.co/guide/en/elasticsearch/reference/7.9/mapping.html
