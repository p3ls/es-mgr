
# Beispielversand an einen UDP - Port

Apps können ihre Logs mit Logstash "als Mittelsmann" ohne Authentifizierung zur Datenbank schicken.

Logstash kann strukturierte Daten verarbeiten, muss aber nicht. 

[Formate](https://www.elastic.co/guide/en/logstash/7.10/codec-plugins.html) können sein:

* plain      # Reads plaintext with no delimiting between events`
* `line       # Reads line-oriented text data`
* `multiline  # Merges multiline messages into a single event`
* `json       # Reads JSON formatted content, creating one event per element in a JSON array`
* `json_lines # Reads newline-delimited JSON`
* `csv        # Takes CSV data, parses it, and passes it along`
* ...

Beispiel in PHP:

```php

<?php

    // line example
    $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

    $msg = "Ping !";
    $len = strlen($msg);

    socket_sendto($sock, $msg, $len, 0, '192.168.253.56', 5047);

    // json example
    $msg = '{"message": "foobar", "token":"a3244aa4f9d9e19821fe3014913cf07fb4cec7c3.1.839316","url":"https:\/\/pay.pferdewetten.de\/process\/safecharge\/checkout\/a3244aa4f9d9e19821fe3014913cf07fb4cec7c3.1.839316","status":200,"description":"OK","request":{"r_url":"https:\/\/pay.pferdewetten.de\/bridge\/auth\/10330595\/1\/Sf726gs6Z\/5.00\/EUR\/?device=mobile","r_cfg":{"url":"https:\/\/pay.pferdewetten.de\/"}}}';
    $len = strlen($msg);

    socket_sendto($sock, $msg, $len, 0, '192.168.253.56', 5048);
    socket_close($sock);

?>

```

Nachrichten aus obigem Beispiel landen im Index `test-udp`. 

Port `5047` erwartet Input-Daten im `line`- Format und Port `5048` als `JSON`. 
Klone das Repo und probier es aus!

    $ php docs/files/send_udp_test.php

Im Dashboard kannst du den Empfang live verfolgen unter:

[http://192.168.253.56:5601/s/test/app/logs/stream](http://192.168.253.56:5601/s/test/app/logs/stream?flyoutOptions=(flyoutId:!n,flyoutVisibility:hidden,surroundingLogsId:!n)&logFilter=(expression:%27message:%20*%27,kind:kuery)&logPosition=(end:now,position:(tiebreaker:12,time:1611055401976),start:now-1d,streamLive:!f))

⚠️ Achtung: Standardmässig sind die `JSON` - Felder nur in der Detailansicht erkennbar. 
Wenn im `JSON` - Objekt kein `message` - Feld existiert ist es in der Logs-Liste (die in erster Linie das Message - Feld anzeigt) unsichtbar. 

![Screenshot JSON Felder im Detail](files/test-udp-detail.png)

Die verwendete Logstsh - Konfiguration sieht so aus:

```ruby

input {
  
  udp {
    port => 5047
    codec => "line"
  }  

  udp {
    port => 5048
    codec => "json" 
  }  
}

output {

  elasticsearch {
    hosts => ["127.0.0.1:9200"]
    ssl => true
    cacert => "/etc/logstash/elasticsearch-ca.pem"
    user => "generic_index_writer"
    password => "xxx"
    index => "test-udp"
  }

}

```