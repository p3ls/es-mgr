
# Transport von bestehenden Log Files

* Eine App schreibt seine eigenen Logfiles, die dann von einem Zusatzprogramm "Filebeat" auf dem gleichen Rechner eingelesen, verarbeitet und vergeschickt werden

Beispiel Kidron-Webserver:

    ┌───────────────────┐         ┌───────────────────┐         ┌─────────────────────────────────────────────────┐
    │                   │         │                   │    │    │                                                 │
    │      Kidron       │         │                   │         │                                                 │
    │                   │────────▶│                   │         │                                                 │
    │                   │         │                   │    │    │                                                 │
    └───────────────────┘         │                   │         │                                                 │
    ┌───────────────────┐         │                   │         │                                                 │
    │                   │         │                   │    │    │                                                 │
    │        PHP        │         │     Filebeat      │         │                    Logstash                     │
    │                   │────────▶│                   │────────▶│                                                 │
    │                   │         │                   │    │    │                                                 │
    └───────────────────┘         │                   │         │                                                 │
    ┌───────────────────┐         │                   │         │                                                 │
    │                   │         │                   │    │    │                                                 │
    │      Apache       │────────▶│                   │         │                                                 │
    │                   │         │                   │         │                                                 │
    │                   │         │                   │    │    │                                                 │
    └───────────────────┘         └───────────────────┘         └─────────────────────────────────────────────────┘
    ┌─────────────────────────────────────────────────┐         ┌─────────────────────────────────────────────────┐
    │                                                 │    │    │                                                 │
    │              192.168.253.153/web02              │         │         192.168.253.56/elastic-stack01          │
    │                                                 │         │                                                 │
    └─────────────────────────────────────────────────┘    │    └─────────────────────────────────────────────────┘

* Logging Driver: [Filebeat](https://www.elastic.co/guide/en/beats/filebeat/master/index.html)

* in diesem Zusammenhang, optionale, andere nützliche Tools 
    + https://rtcamp.com/tutorials/php/fpm-status-page/
    + https://github.com/kozlice/phpfpmbeat
    + Kibana, Apache Logs Module (TODO: nochmal genau nachlesen was das kann)
                                                                                                               
Filebeat Config auf dem Webserver:

```yaml
---
filebeat.inputs:
- type: log
  enabled: true
  paths: 
    - /var/www/htdocs/kidron/app/application/logs/*.php
    - /optional/path/to/apache/or/php/error/*.log
  fields:
    app: kidron-prod
    
processors:
 - drop_fields:
      fields: ["agent", "log", "input", "ecs"]
      
output.logstash:
  hosts: ["192.168.253.56:5044"]
```

Beispiel Logstash-Config:

```ruby
input {
  beats {
    port => 5044
  }  
}

output {

  elasticsearch {
    hosts => ["127.0.0.1:9200"]
    ssl => true
    cacert => "/etc/logstash/elasticsearch-ca.pem"
    user => "writer"
    password => "xxx"

    ecs_compatibility => "disabled"
    manage_template => false      
    ilm_enabled => false

    index => "%{[fields][app]}"

  } 
}
```
